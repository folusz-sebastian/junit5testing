# JUnit 5 Testing

This project contains exercises from
[udemy course](https://www.udemy.com/share/101yKuBEQZcFhUQXg=/)
From this course I have learned such things as:

- Mockito 2
- Test Driven Development (TDD)
- Hamcrest Matchers
- testing collections
- testing exceptions
- test patterns and good practices

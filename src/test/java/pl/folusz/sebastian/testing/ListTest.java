package pl.folusz.sebastian.testing;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

public class ListTest {

    @Test
    void testIfTwoListsAreTheSame() {
        Meal meal1 = new Meal(13, "nazwa1");
        Meal meal2 = new Meal(13, "nazwa2");

        List<Meal> meals1 = Arrays.asList(meal1, meal2);
        List<Meal> meals2 = Arrays.asList(meal2, meal1);

        assertThat(meals1, not(is(meals2)));
    }
}

package pl.folusz.sebastian.testing.cart;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.folusz.sebastian.testing.Order;
import pl.folusz.sebastian.testing.OrderStatus;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CartServiceTest {
    @InjectMocks
    private CartService cartService;
    @Mock
    private CartHandler cartHandler;
    @Captor
    private ArgumentCaptor<Cart> argumentCaptor;

    @Test
    void processCartShouldSendToPrepare() {
        Order order = new Order();
        Cart cart = new Cart();
        cart.addOrderToCart(order);

        when(cartHandler.canHandleCart(cart)).thenReturn(true);

        Cart processedCart = cartService.processedCart(cart);

        verify(cartHandler).sendToPrepare(cart);
        assertThat(processedCart.getOrders(), hasSize(1));
        InOrder inOrder = inOrder(cartHandler);
        inOrder.verify(cartHandler).canHandleCart(cart);
        inOrder.verify(cartHandler).sendToPrepare(cart);
    }

    @Test
    void processCartShouldNotSendToPrepare() {
        Order order = new Order();
        Cart cart = new Cart();
        cart.addOrderToCart(order);

        when(cartHandler.canHandleCart(cart)).thenReturn(false);

        verify(cartHandler, never()).sendToPrepare(cart);
        Cart processedCart = cartService.processedCart(cart);
        assertThat(processedCart.getOrders().get(0).getOrderStatus(), equalTo(OrderStatus.REJECTED));
    }

    @Test
    void processCartShouldNotSendToPrepareWithArgumentMatchers() {
        Order order = new Order();
        Cart cart = new Cart();
        cart.addOrderToCart(order);

        when(cartHandler.canHandleCart(any(Cart.class))).thenReturn(false);

        Cart processedCart = cartService.processedCart(cart);

        verify(cartHandler, never()).sendToPrepare(any(Cart.class));
        assertThat(processedCart.getOrders(), hasSize(1));
    }

    @Test
    void processCartShouldSendToPrepareWithLambdas() {
        Order order = new Order();
        Cart cart = new Cart();
        cart.addOrderToCart(order);

        when(cartHandler.canHandleCart(argThat(c -> c.getOrders().size() > 0))).thenReturn(true);

        Cart processedCart = cartService.processedCart(cart);

        verify(cartHandler).sendToPrepare(cart);
        assertThat(processedCart.getOrders(), hasSize(1));
        InOrder inOrder = inOrder(cartHandler);
        inOrder.verify(cartHandler).canHandleCart(cart);
        inOrder.verify(cartHandler).sendToPrepare(cart);
    }

    @Test
    void processCartShouldThrowException() {
        Order order = new Order();
        Cart cart = new Cart();
        cart.addOrderToCart(order);

        when(cartHandler.canHandleCart(cart)).thenThrow(IllegalStateException.class);

        assertThrows(IllegalStateException.class, () -> cartService.processedCart(cart));
    }

    @Test
    void processCartShouldSendToPrepareWithArgumentCaptor() {
        Order order = new Order();
        Cart cart = new Cart();
        cart.addOrderToCart(order);

        when(cartHandler.canHandleCart(cart)).thenReturn(true);

        Cart processedCart = cartService.processedCart(cart);

        verify(cartHandler).sendToPrepare(argumentCaptor.capture());

        assertThat(argumentCaptor.getValue().getOrders().size(), equalTo(1));
        assertThat(processedCart.getOrders(), hasSize(1));
    }

    @Test
    void shouldDoNothingWhenProcessCart() {
        Order order = new Order();
        Cart cart = new Cart();
        cart.addOrderToCart(order);

        when(cartHandler.canHandleCart(cart)).thenReturn(true);
        doNothing().when(cartHandler).sendToPrepare(cart);

        //kolejne  wywolania metody
        willDoNothing().willThrow(IllegalStateException.class).given(cartHandler).sendToPrepare(cart);

        Cart processedCart = cartService.processedCart(cart);

        assertThat(processedCart.getOrders(), hasSize(1));
    }

    @Test
    void showAnswerWhenProcessCart() {
        Order order = new Order();
        Cart cart = new Cart();
        cart.addOrderToCart(order);

        when(cartHandler.canHandleCart(cart)).then(i ->
                {
                    Cart argumentCart = i.getArgument(0);
                    argumentCart.clearCart();
                    return true;
                }
        );

        Cart processedCart = cartService.processedCart(cart);

        assertThat(processedCart.getOrders(), hasSize(1));
    }
}
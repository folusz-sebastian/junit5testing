package pl.folusz.sebastian.testing.cart;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pl.folusz.sebastian.testing.Order;
import pl.folusz.sebastian.testing.cart.Cart;

import java.time.Duration;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Test cases for Cart")
class CartTest {

    private Cart cart;

    @BeforeEach
    void instantiateCart() {
        this.cart = new Cart();
    }

    @Test
    @DisplayName("Cart is able to process 1000 orders in 100 ms")
    void simulateLargeOrder() {
        assertTimeout(Duration.ofMillis(200), () -> cart.simulateLargeOrder());
    }

    @Test
    void cardShouldNotBeEmptyAfterAddingOrderToCart(){
        Order order = new Order();

        this.cart.addOrderToCart(order);

        assertThat(cart.getOrders(), anyOf(
                notNullValue(),
                hasSize(1),
                is(not(empty())),
                is(not(emptyCollectionOf(Order.class)))
        ));

        assertThat(cart.getOrders(), allOf(
                notNullValue(),
                hasSize(1),
                is(not(empty())),
                is(not(emptyCollectionOf(Order.class)))
        ));

        assertAll(
                () -> assertThat(cart.getOrders(), notNullValue()),
                () -> assertThat(cart.getOrders(), hasSize(1)),
                () -> assertThat(cart.getOrders(), is(not(empty()))),
                () -> assertThat(cart.getOrders(), is(not(emptyCollectionOf(Order.class))))
        );
    }
}
package pl.folusz.sebastian.testing;

import org.junit.jupiter.api.*;

import java.io.IOException;

class OrderBackupTest {

    private static OrderBackup orderBackup;

    @BeforeAll
    static void setup() throws IOException {
        orderBackup = new OrderBackup();
        orderBackup.createFile();
    }

    @AfterAll
    static void tearDown() throws IOException {
        orderBackup.closeFile();
    }

    @BeforeEach
    void appendTextAtTheBeggingOfTheLine() throws IOException {
        orderBackup.getWriter().append("Order: ");
    }

    @AfterEach
    void appendTextAtTheEndOfTheLine() throws IOException {
        orderBackup.getWriter().append("backed up");
    }

    @Test
    void backupOrderWithOneMeal() throws IOException {
        Meal meal = new Meal(24, "Fries");
        OrderedMeal orderedMeal = new OrderedMeal(meal, 13);
        Order order = new Order();
        order.addMealToOrder(orderedMeal);

        orderBackup.backupOrder(order);

        System.out.println("Order: " + order.toString() + " backed up.");
    }

}
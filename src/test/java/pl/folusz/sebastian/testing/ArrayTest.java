package pl.folusz.sebastian.testing;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class ArrayTest {

    @Test
    void testAssertArrayEquals() {
        int[] array1 = {1,2,3};
        int[] array2 = {1,2,3};

        assertArrayEquals(array1, array2);
    }
}

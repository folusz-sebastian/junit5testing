package pl.folusz.sebastian.testing;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class MealTest {

    @Test
    void shouldReturnDiscountedPrice() {
        Meal meal = new Meal(35, "pizza");

        int discountedPrice = meal.discountedPrice(7);

        assertEquals(28, discountedPrice);
        assertThat(discountedPrice, equalTo(28));
    }

    @Test
    void mealsAreTheSameWhenPricesAndNamesAreTheSame() {
        Meal meal1 = new Meal(35, "pizza");
        Meal meal2 = new Meal(35, "pizza");

        assertEquals(meal1, meal2);
        assertThat(meal1, sameInstance(meal2));
    }

    @Test
    void exceptionShouldBeThrownWhenDiscountIsHigherThanPrice() {
        Meal meal = new Meal(15, "posilek");

        assertThrows(IllegalArgumentException.class, ()-> meal.discountedPrice(30));
    }

    @ParameterizedTest
    @ValueSource(ints = {4, 7, 15, 18})
    void mealPriceShouldBeLowerThan20(int price) {
        assertThat(price, lessThan(20));
    }

    @ParameterizedTest
    @MethodSource("nameAndPriceOfMealStream")
    void burgersShouldHaveCorrectNamesAndPrice(String name, int price) {
        assertThat(name, containsString("burger"));
        assertThat(price, greaterThanOrEqualTo(10));
    }

    private static Stream<Arguments> nameAndPriceOfMealStream() {
        return Stream.of(
                Arguments.of("Hamburger", 10),
                Arguments.of("Cheeseburger", 34)
        );
    }

    @ParameterizedTest
    @MethodSource("mealNamesStream")
    void cakeNamesShouldEndWithCake(String name) {
        assertThat(name, notNullValue());
        assertThat(name, endsWith("cake"));
    }

    private static Stream<String> mealNamesStream() {
        List<String> cakeNames = Arrays.asList("coscake", "pancake", "fruitcake");
        return cakeNames.stream();
    }
}
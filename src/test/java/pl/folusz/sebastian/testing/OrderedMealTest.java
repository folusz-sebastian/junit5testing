package pl.folusz.sebastian.testing;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.function.Executable;

import java.util.ArrayList;
import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.lessThan;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

class OrderedMealTest {

    private Order order;

    @BeforeEach
    void initializeOrder() {
        this.order = new Order();
    }

    @TestFactory
    Collection<DynamicTest> wholePriceForOrderedMeal() {
        this.order.addMealToOrder(new OrderedMeal(new Meal(13, "Hamburger"), 2));
        this.order.addMealToOrder(new OrderedMeal(new Meal(13, "Hamburger"), 3));
        this.order.addMealToOrder(new OrderedMeal(new Meal(13, "Hamburger"), 4));

        Collection<DynamicTest> dynamicTests = new ArrayList<>();

        for (int i = 0; i < order.getOrderedMeals().size(); i++) {
            OrderedMeal orderedMeal = order.getOrderedMeals().get(i);
            Executable executable = () -> {
                assertThat(orderedMeal.wholePriceForOrderedMeal(), lessThan(53));
            };
            dynamicTests.add(DynamicTest.dynamicTest("test " + i, executable));
        }
        return dynamicTests;
    }

    @Test
    void testMealSumPriceWithSpy() {
//        Meal meal = new Meal(12, "name");
        OrderedMeal orderedMeal = new OrderedMeal(new Meal(12, "name"), 2);
        OrderedMeal orderedMealSpy = spy(orderedMeal);
//        Meal mealSpy = spy(Meal.class);

//        when(orderedMealSpy.getQuantity()).thenReturn(2);
//        when(mealSpy.getPrice()).thenReturn(12);
//        when(orderedMealSpy.getMeal()).thenReturn(meal);

        int wholePriceForOrderedMeal = orderedMealSpy.wholePriceForOrderedMeal();

        assertThat(wholePriceForOrderedMeal, equalTo(24));
    }
}
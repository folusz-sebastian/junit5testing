package pl.folusz.sebastian.testing.account;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AccountServiceTest {
    @Test
    void allActiveAccounts() {
        AccountRepository accountRepository = mock(AccountRepository.class);
        AccountService accountService = new AccountService(accountRepository);
        when(accountRepository.allAccounts()).thenReturn(sampleAccountData());

        List<Account> accounts = accountService.allActiveAccounts();

        assertThat(accounts, Matchers.hasSize(2));
    }

    private List<Account> sampleAccountData() {
        Address address1 = new Address("street", 1);
        Account account1 = new Account(address1);

        Account account2 = new Account();

        Address address2 = new Address("street2", 2);
        Account account3 = new Account(address2);

        return Arrays.asList(account1, account2, account3);
    }
}

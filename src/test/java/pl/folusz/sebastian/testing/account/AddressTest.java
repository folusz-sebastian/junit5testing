package pl.folusz.sebastian.testing.account;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.jupiter.api.Assertions.*;

class AddressTest {

    @ParameterizedTest
    @CsvSource({"ulica, 10", "ulica2, 30"})
    void givenAddressesShouldHaveProperNamesAndNumbers(String streetName, int number) {
        assertThat(streetName, notNullValue());
        assertThat(streetName, containsString("a"));
        assertThat(number, notNullValue());
        assertThat(number, lessThan(40));
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/addresses.csv")
    void addressesFromCsvFileShouldHaveProperNamesAndNumbers(String streetName, int number) {
        assertThat(streetName, notNullValue());
        assertThat(streetName, containsString("a"));
        assertThat(number, notNullValue());
        assertThat(number, lessThan(40));
    }

}
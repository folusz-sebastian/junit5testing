package pl.folusz.sebastian.testing.account;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumingThat;

public class AccountTest {

    @Test
    public void accountShouldNotBeActiveOnStart() {
        Account account = new Account();
        assertFalse(account.isActive());
        assertThat(account.isActive(), is(false));
    }

    @Test
    public void accountShouldBeActiveAfterStart() {
        Account account = new Account();
        account.activate();
        assertTrue(account.isActive());
    }

    @Test
    public void AddressShouldNotExistAtTheBeginning() {
        Account account = new Account();

        Address accountAddress = account.getAddress();

        assertThat(accountAddress, nullValue());
    }

    @RepeatedTest(5)
    void newAccountWithNotNullAddressShouldBeActive() {
        Address address = new Address();
        Account account = new Account(address);

        assumingThat(
                address != null,
                () -> assertThat(account.isActive(), is(true))
        );
    }
}

package section4Homework;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.stream.Stream;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class CoordinatesTest {

    @ParameterizedTest
    @MethodSource("coordinatesSamples")
    void coordinatesShouldBeLessThan100AndGreaterThan0(int x, int y) {
        Coordinates coordinates = new Coordinates(x, y);
        assertAll(() -> {
            assertThat(coordinates.getX(), Matchers.lessThan(100));
            assertThat(coordinates.getX(), Matchers.greaterThan(0));
            assertThat(coordinates.getY(), Matchers.lessThan(100));
            assertThat(coordinates.getY(), Matchers.greaterThan(0));
        });
    }

    private static Stream<Arguments> coordinatesSamples() {
        return Stream.of(
          Arguments.of(34,56),
          Arguments.of(5,30)
        );
    }

    @Test
    void constructorShouldFailWhenAnyCoordinateAbove100() {
        assertThrows(IllegalArgumentException.class, () -> new Coordinates(101, 102));
        assertThrows(IllegalArgumentException.class, () -> new Coordinates(101, 50));
        assertThrows(IllegalArgumentException.class, () -> new Coordinates(30, 101));
    }

    @Test
    void constructorShouldFailWhenAnyCoordinateBelow0() {
        assertThrows(IllegalArgumentException.class, () -> new Coordinates(-3, -2));
        assertThrows(IllegalArgumentException.class, () -> new Coordinates(34, -3));
        assertThrows(IllegalArgumentException.class, () -> new Coordinates(-23, 36));
    }

    @Test
    void copyOfCoordinatesShouldReturnNewInstance() {
        Coordinates coordinates = new Coordinates(10, 10);
        Coordinates coordinatesCopy = Coordinates.copy(coordinates, 0, 0);

        assertThat(coordinatesCopy, not(sameInstance(coordinates)));
        assertThat(coordinatesCopy, equalTo(coordinates));
    }

    @Test
    void copyOfCoordinatesShouldAddNewCoordinates() {
        Coordinates coordinates = new Coordinates(10, 10);
        Coordinates coordinatesCopy = Coordinates.copy(coordinates, 4, 5);

        assertThat(coordinatesCopy.getX(), equalTo(14));
        assertThat(coordinatesCopy.getY(), equalTo(15));
    }
}

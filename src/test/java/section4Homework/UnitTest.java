package section4Homework;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class UnitTest {

    @Test
    void loadingCargoWithWeightGraterThanMaxShouldFail() {
        Unit unit = new Unit(new Coordinates(0, 0), 130, 200);
        Cargo cargo = new Cargo("name", 220);

        assertThrows(IllegalStateException.class, () -> unit.loadCargo(cargo));
    }

    @Test
    void loadingCargoShouldCalculateCorrectlyWholeCargoWeight() {
        Unit unit = new Unit(new Coordinates(0, 0), 130, 200);
        Cargo cargo = new Cargo("name", 100);
        unit.loadCargo(cargo);
        assertThat(cargo.getWeight(), equalTo(100));
    }

    @Test
    void unloadCargo() {
        Unit unit = new Unit(new Coordinates(0, 0), 130, 200);
        Cargo cargo = new Cargo("name", 20);
        unit.loadCargo(cargo);
        unit.unloadCargo(cargo);
        assertThat(unit.getCargoList(), not(contains(cargo)));
    }

    @Test
    void unloadAllCargo() {
        Unit unit = new Unit(new Coordinates(0, 0), 130, 200);
        unit.loadCargo(new Cargo("name", 20));
        unit.unloadAllCargo();

        assertThat(unit.getLoad(), equalTo(0));
        assertThat(unit.getCargoList(), empty());
    }
}
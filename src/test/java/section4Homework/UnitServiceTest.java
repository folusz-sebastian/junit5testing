package section4Homework;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.NoSuchElementException;
import java.util.Optional;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

//@MockitoSettings(strictness = Strictness.WARN)
@ExtendWith(MockitoExtension.class)
public class UnitServiceTest {
    @InjectMocks
    private UnitService unitService;
    @Mock
    private UnitRepository unitRepository;
    @Mock
    private CargoRepository cargoRepository;

    @Test
    void loadingCargoTesting() {
        //given
        Unit unit = new Unit(new Coordinates(0, 0), 100, 1000);
        String cargoName = "cargoName";
        Cargo cargo = new Cargo(cargoName, 13);

        when(cargoRepository.findCargoByName(cargoName))
                .thenReturn(Optional.of(cargo));
        //when
        unitService.addCargoByName(unit, cargoName);
        //then
        verify(cargoRepository).findCargoByName(cargoName);
        assertThat(unit.getCargoList(), hasSize(1));
        assertThat(unit.getLoad(), is(13));
        assertThat(unit.getCargoList().get(0).getName(), equalTo(cargoName));
    }

    @Test
    void throwExceptionWhenCargoIsNotFound() {
        //given
        Unit unit = new Unit(new Coordinates(0, 0), 100, 1000);
        String cargoName = "cargoName";

        when(cargoRepository.findCargoByName(anyString())).thenReturn(Optional.empty());
        //when
        //then
        assertThrows(NoSuchElementException.class, () -> unitService.addCargoByName(unit, cargoName));
    }

    @Test
    void shouldReturnUnitByCoordinatesWhenGettingUnitOn() {
        //given
        Coordinates coordinates = new Coordinates(0, 0);
        Unit unit = new Unit(coordinates, 100, 1000);
        when(unitRepository.getUnitByCoordinates(coordinates)).thenReturn(unit);
        //when
        Unit unitOn = unitService.getUnitOn(coordinates);

        //then
        verify(unitRepository).getUnitByCoordinates(coordinates);
        assertThat(unit, sameInstance(unitOn));
    }

    @Test
    void shouldThrowExceptionWhenUnitIsNotFound() {
        //given
        Coordinates coordinates = new Coordinates(0, 0);
        given(unitRepository.getUnitByCoordinates(coordinates)).willReturn(null);

        //then
        assertThrows(NoSuchElementException.class, () -> unitService.getUnitOn(coordinates));
    }
}

package section4Homework;

import java.util.NoSuchElementException;
import java.util.Optional;

public class UnitService {
    private CargoRepository cargoRepository = new CargoRepository();
    private UnitRepository unitRepository = new UnitRepository();

    void addCargoByName(Unit unit, String name) {
        Optional<Cargo> cargoByName = cargoRepository.findCargoByName(name);

        if (cargoByName.isPresent()) {
            unit.loadCargo(cargoByName.get());
        } else {
            throw  new NoSuchElementException("unable to find");
        }
    }

    Unit getUnitOn(Coordinates coordinates) {
        Unit unitByCoordinates = unitRepository.getUnitByCoordinates(coordinates);

        if (unitByCoordinates == null) {
            throw new NoSuchElementException("unable to find");
        } else {
            return unitByCoordinates;
        }
    }
}

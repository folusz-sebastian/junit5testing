package section4Homework;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Unit {
    private static Random random = new Random();

    private Coordinates coordinates;
    private int fuel;
    private int maxFuel;
    private List<Cargo> cargoList;
    private int maxCargoWeight;
    private int currentWholeCargoWeight;

    Unit(Coordinates startCoordinates, int maxFuel, int maxCargoWeight) {
        this.coordinates = startCoordinates;
        this.maxFuel = maxFuel;
        this.fuel = maxFuel;
        this.maxCargoWeight = maxCargoWeight;
        this.currentWholeCargoWeight = 0;
        this.cargoList = new ArrayList<>();
    }

    Coordinates move(int x, int y) {

        if (this.fuel - (x + y) < 0) {
            throw new IllegalStateException("Unit cannot move that far");
        }

        Coordinates coordinatesAfterMove = Coordinates.copy(this.coordinates, x, y);
        this.coordinates = coordinatesAfterMove;

        this.fuel = this.fuel - (x + y);

        return coordinatesAfterMove;
    }

    void tankUp() {
        int restPoints = random.nextInt(10);

        if (restPoints + this.fuel >= maxFuel) {
            this.fuel = maxFuel;
        } else {
            this.fuel += restPoints;
        }
    }

    void loadCargo(Cargo cargo) {

        if (currentWholeCargoWeight + cargo.getWeight() > maxCargoWeight) {
            throw new IllegalStateException("Can not load any more cargoList");
        }

        this.cargoList.add(cargo);

        this.currentWholeCargoWeight = calculateWholeCargoWeight();

    }

    void unloadCargo(Cargo cargo) {
        this.cargoList.remove(cargo);
        this.currentWholeCargoWeight = calculateWholeCargoWeight();
    }

    void unloadAllCargo() {
        cargoList.clear();
        this.currentWholeCargoWeight = 0;
    }

    private int calculateWholeCargoWeight() {
        return cargoList.stream().mapToInt(Cargo::getWeight).sum();
    }

    int getFuel() {
        return this.fuel;
    }

    int getLoad() {
        return this.currentWholeCargoWeight;
    }

    public List<Cargo> getCargoList() {
        return cargoList;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }
}

package pl.folusz.sebastian.testing;

public class OrderedMeal {
    private Meal meal;
    private int quantity;

    public OrderedMeal() {
    }

    public OrderedMeal(Meal meal) {
        this.meal = meal;
    }

    public OrderedMeal(Meal meal, int quantity) {
        this.meal = meal;
        this.quantity = quantity;
    }

    public int wholePriceForOrderedMeal() {
        return this.meal.getPrice() * this.quantity;
    }

    public Meal getMeal() {
        return meal;
    }

    public int getQuantity() {
        return quantity;
    }
}

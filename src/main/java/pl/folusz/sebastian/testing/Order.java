package pl.folusz.sebastian.testing;

import java.util.ArrayList;
import java.util.List;

public class Order {
    private OrderStatus orderStatus;
    private List<OrderedMeal> orderedMeals = new ArrayList<>();

    public void addMealToOrder(OrderedMeal orderedMeal) {
        this.orderedMeals.add(orderedMeal);
    }

    public void removeMealFromOrder(OrderedMeal orderedMeal) {
        this.orderedMeals.remove(orderedMeal);
    }

    public void cancel() {
        this.orderedMeals.clear();
    }

    public void changeOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public List<OrderedMeal> getOrderedMeals() {
        return orderedMeals;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderedMeals=" + orderedMeals +
                '}';
    }
}

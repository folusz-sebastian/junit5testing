package pl.folusz.sebastian.testing;

public enum OrderStatus {
    ORDERED,
    DELIVERED,
    READY,
    PREPARING,
    REJECTED
}

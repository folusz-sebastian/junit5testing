package pl.folusz.sebastian.testing.account;

import java.util.List;

public interface AccountRepository {
    List<Account> allAccounts();
}

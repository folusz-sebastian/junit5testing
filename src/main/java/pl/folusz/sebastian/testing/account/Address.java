package pl.folusz.sebastian.testing.account;

public class Address {
    private String streetName;
    private int number;

    public Address() {
    }

    public Address(String streetName, int number) {
        this.streetName = streetName;
        this.number = number;
    }
}

package pl.folusz.sebastian.testing.account;

public class Account {
    private boolean active;
    private Address address;

    public Account() {
        this.active = false;
    }

    public Account(Address address) {
        this.address = address;

        if(address != null)
            activate();
        else
            disActivate();
    }

    public void activate() {
        this.active = true;
    }

    private void disActivate() {
        this.active = false;
    }

    public boolean isActive() {
        return active;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}

package pl.folusz.sebastian.testing.cart;

import pl.folusz.sebastian.testing.Meal;
import pl.folusz.sebastian.testing.Order;
import pl.folusz.sebastian.testing.OrderedMeal;

import java.util.ArrayList;
import java.util.List;

public class Cart {
    private List<Order> orders = new ArrayList<>();

    public void addOrderToCart(Order order) {
        orders.add(order);
    }

    public void clearCart() {
        orders.clear();
    }

    public void simulateLargeOrder() {
        for (int i=0; i<10000; i++) {
            Meal meal = new Meal(15, "French fries");
            OrderedMeal orderedMeal = new OrderedMeal(meal, 2);
            Order order = new Order();
            order.addMealToOrder(orderedMeal);
            addOrderToCart(order);
        }
        System.out.println("Cart size: " + orders.size());
        clearCart();
    }

    public List<Order> getOrders() {
        return orders;
    }
}

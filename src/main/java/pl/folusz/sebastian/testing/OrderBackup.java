package pl.folusz.sebastian.testing;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class OrderBackup {
    private BufferedWriter writer;

    public void createFile() throws IOException {
        String fileName = "orderBackup.txt";
        FileWriter fileWriter = new FileWriter(fileName);

        writer = new BufferedWriter(fileWriter);
    }

    public void backupOrder(Order order) throws IOException {
        writer.append(order.toString());
    }

    public void closeFile() throws IOException {
        writer.close();
    }

    public Writer getWriter() {
        return writer;
    }
}
